import {Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {User} from './user.entity';
import {UserService} from './user/user.service';
import {JwtModule} from "@nestjs/jwt";
import {AuthService} from './auth/auth.service';
import {AuthController} from './auth/auth.controller';

@Module({
    imports: [
        TypeOrmModule.forFeature([User]), JwtModule.register({
            secretOrPrivateKey: 'secret123456789'
        }),
        TypeOrmModule.forRoot({
            type: "mysql",
            host: "127.0.0.1",
            port: 3306,
            username: "root",
            password: "",
            database: "users",
            entities: [__dirname + '/**/*.entity{.ts,.js}'],
            synchronize: true,
            })],

    providers: [UserService, AuthService],
    controllers: [AuthController]
})
export class AuthModule {
}
