import {Injectable} from '@angular/core';
import {ValidationErrors} from '@angular/forms';
import {Observable} from 'rxjs';
import {delay} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private users: string[];

    constructor() {
        this.users = ['john', 'ivan', 'anna'];
    }

    /* Запрос валидации */
    validateName(userName: string): Observable<ValidationErrors> {
        return new Observable<ValidationErrors>(observer => {
            const user = this.users.find(user => user === userName);
            /* Eсли пользователь есть в массиве, то возвращаем ошибку */
            if (user) {
                observer.next({
                    nameError: 'Пользователь с таким именем уже существует'
                });
                observer.complete();
            }

            /* Если пользователя нет, то валидация успешна */
            observer.next(null);
            observer.complete();

        }).pipe(delay(1000));
    }
}
