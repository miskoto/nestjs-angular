import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {UserService} from '../service/user.service';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

    AuthForm: FormGroup;

    constructor(private fb: FormBuilder, private userValidation: UserService) {
    }

    ngOnInit() {
        this.initForm();
    }

    public onSubmit(): void {
        const controls = this.AuthForm.controls;
        Object.keys(controls).forEach(key => {
            controls[key].markAllAsTouched();
        });
    }

    // Инициализация формы
    private initForm(): void {
        this.AuthForm = this.fb.group({
            username: ['', [
                Validators.required,
                Validators.pattern(/^[A-z0-9]*$/),
                Validators.minLength(5)],
                [this.nameAsyncValidator.bind(this)]
            ],
            email: ['', [
                Validators.required,
                Validators.email
            ]],
            password: ['', [
                Validators.required,
                this.passwordValidator
            ]]
        });
    }

    /*Валидатор для пароля */
    private passwordValidator(control: FormControl): ValidationErrors {
        const value = control.value;

        /*Проверка на содержание цифр*/
        const hasNumber = /[0-9]/.test(value);

        /*Проверка на содержание заглавных букв*/
        const hasCapitalLetter = /[A-Z]/.test(value);

        /*Проверка на содержание прописных букв*/
        const hasLowercaseLetter = /[a-z]/.test(value);

        /*Проверка на минимальную длину пароля*/
        const isLengthValid = value ? value.length > 7 : false;

        /*Общая проверка*/
        const passwordValid = hasNumber && hasCapitalLetter && hasLowercaseLetter && isLengthValid;
        if (!passwordValid) {
            return {invalidPassword: 'Password is not valid'};
        }
        return null;
    }
    /*Асинхронный валидатор*/

    nameAsyncValidator(control: FormControl): Observable<ValidationErrors> {
        return this.userValidation.validateName(control.value);
    }

}
